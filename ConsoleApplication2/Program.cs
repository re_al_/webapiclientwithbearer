﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace ConsoleApplication2
{
    class Program
    {
        static HttpClient client = new HttpClient();
        static void Main(string[] args)
        {
            RunAsync().Wait();
        }

        static async Task RunAsync()
        {
            try
            {
                client.BaseAddress = new Uri("http://200.105.174.10/extranjeria/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Get Token
                TokenResponseModel token = await GetTokenAsync();
                Console.WriteLine($"Token at {token.AccessToken}");

                //Make the call
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token.AccessToken);

                //make request
                HttpResponseMessage response = await client.GetAsync("API/segusuarios");
                if (response.IsSuccessStatusCode)
                {
                    var responseString = await response.Content.ReadAsStringAsync();
                    Console.WriteLine("response: {0}", responseString);
                }
                Console.ReadLine();
            }
            catch (Exception exp)
            {
                Console.WriteLine(exp.Message);
                Console.WriteLine(exp.StackTrace);
                Console.ReadLine();
            }
        }        

        static async Task<TokenResponseModel> GetTokenAsync()
        {
            var values = new Dictionary<string, string>();
            values.Add("grant_type", "password");
            values.Add("username", "alo");
            values.Add("password", "vera");
            var loginContent = new FormUrlEncodedContent(values);

            //HttpResponseMessage response = await client.PostAsJsonAsync("token", usuario);
            //response.EnsureSuccessStatusCode();
            HttpResponseMessage responseMessage = await client.PostAsync("Token", loginContent);

            // return URI of the created resource.
            if (responseMessage.IsSuccessStatusCode)
            {
                string jsonMessage;
                using (Stream responseStream = await responseMessage.Content.ReadAsStreamAsync())
                {
                    jsonMessage = new StreamReader(responseStream).ReadToEnd();
                }

                TokenResponseModel tokenResponse = (TokenResponseModel)JsonConvert.DeserializeObject(jsonMessage, typeof(TokenResponseModel));

                return tokenResponse;
            }
            else
            {
                return null;
            }
        }
    }



    class TokenResponseModel
    {
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }

        [JsonProperty("token_type")]
        public string TokenType { get; set; }

        [JsonProperty("expires_in")]
        public int ExpiresIn { get; set; }

        [JsonProperty("userName")]
        public string Username { get; set; }

        [JsonProperty(".issued")]
        public string IssuedAt { get; set; }

        [JsonProperty(".expires")]
        public string ExpiresAt { get; set; }
    }
}
